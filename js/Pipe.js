function Pipe(pipe_up,pipe_down,step,x){
	this.pipe_up=pipe_up;
	this.pipe_down=pipe_down;
	this.step=step;
	this.x=x;
	this.count=0;
	this.height= parseInt(Math.random()*248)+1;
	this.road=parseInt(Math.random()*50)+100;
}
Pipe.prototype.createPipe=function(){
	return new Pipe(this.pipe_up,this.pipe_down,this.step,this.x);
}