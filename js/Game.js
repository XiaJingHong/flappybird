function Game(ctx,bird,pipe,land,mountain){
	this.ctx=ctx;
	this.bird=bird;
	this.pipe=[pipe];
	this.land=land;
	this.mountain=mountain;
	this.timer=null;
	this.frame=null;
	this.test=true;
	this.init();
}

Game.prototype.init=function(){
	this.start();
	this.bindEvent();
}

Game.prototype.start=function(){
	var me=this;
	this.timer=setInterval(function(){
		me.frame++;
		me.clear();
		me.renderMountain();
		me.renderPipe();
		me.renderLand();
		me.renderBird();
		me.bird.falldown();
		if(me.bird.state==="D"){
			if(!(me.frame%10)){
				me.bird.fly();
			}
		}else{
			if(!(me.frame%3)){
				me.bird.fly();
			}
		}
		if(!(me.frame%60)){
			me.createPipe();
		}
		me.checkPipe();
		me.checkLand();
	},20)

}

Game.prototype.clear=function(){
	this.ctx.clearRect(0, 0, 360, 512);
}
Game.prototype.renderMountain=function(){
	var img=this.mountain.img;
	this.mountain.x-=this.mountain.step;
	if(this.mountain.x<-img.width){
		this.mountain.x=0;
	}
	this.ctx.drawImage(img,this.mountain.x,this.mountain.y);
	this.ctx.drawImage(img,this.mountain.x+img.width,this.mountain.y);
	this.ctx.drawImage(img,this.mountain.x+img.width*2,this.mountain.y);
}
Game.prototype.renderLand=function(){
	var img=this.land.img;
	this.land.x-=this.land.step;
	if(this.land.x<-img.width){
		this.land.x=0;
	}
	this.ctx.drawImage(img,this.land.x,this.land.y);
	this.ctx.drawImage(img,this.land.x+img.width,this.land.y);
	this.ctx.drawImage(img,this.land.x+img.width*2,this.land.y);
}
Game.prototype.renderPipe=function(){
	var me=this;
	this.pipe.forEach(function(value,index){
		value.count++;
		value.x=me.ctx.canvas.width-value.step*value.count;
		me.ctx.drawImage(value.pipe_up,value.x,value.height-value.pipe_up.height);
		me.ctx.drawImage(value.pipe_down,value.x,value.height+value.road);
		// test
		if(me.test){			
			me.ctx.strokeRect(value.x,0,value.pipe_up.width,value.height);
			me.ctx.strokeRect(value.x,value.height+value.road,value.pipe_down.width,me.ctx.canvas.height-value.road-value.height);
		}
	})
}
Game.prototype.createPipe=function(){
	var pipe=this.pipe[0].createPipe();
	// console.log(pipe.road);
	this.pipe.push(pipe);
}
Game.prototype.removePipe=function(){
	this.pipe.shift();
}
Game.prototype.renderBird=function(){
	var img=this.bird.img;
	this.ctx.save();

	this.ctx.translate(this.bird.x, this.bird.y);
	var deg=this.bird.state==="D"?1:-1;
	this.ctx.rotate((Math.PI/180)*this.bird.speed*deg);
	this.ctx.drawImage(img,-img.width/2,-img.height/2);

	this.ctx.restore();
}
Game.prototype.bindEvent=function(){
	var me=this;
	this.ctx.canvas.onclick=function(){
		// console.log(1);
		// if(me.timer=null){
		// 	me.start();
		// }else{
			me.bird.goUp();
		// }
	}
}

Game.prototype.die=function(){
	clearInterval(this.timer);
}
Game.prototype.checkLand=function(){
	var l_h=this.ctx.canvas.height-this.land.img.height-this.bird.img.height/3;
	if(this.bird.y>l_h){
		this.die();
	}
}
Game.prototype.checkPipe=function(){
	var me=this;
	var b_x1=me.bird.x-me.bird.img.width/3;
	var b_x2=me.bird.x+me.bird.img.width/3;
	var b_y1=me.bird.y-me.bird.img.height/3;
	var b_y2=me.bird.y+me.bird.img.height/3;
	
	// test
	if(this.test){
		this.ctx.strokeRect(b_x1,b_y1,b_x2-b_x1,b_y2-b_y1);
	}

	this.pipe.forEach(function(value,index){
		if(value.x<=-60){
			me.removePipe();
			// console.log(me.pipe);
		}
		// console.log(value.height,b_y1,b_y2);
		if(value.x<b_x2&&(value.x+me.bird.img.width/3)>b_x1){
			if(!(value.height<b_y1&&(value.height+value.road)>b_y2)){
				me.die();
			}
		}
	})
}